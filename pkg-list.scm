(package (wak-ssax (5 1) (1))
  (depends (srfi-13)
           (srfi-48)
           (wak-common))
  
  (synopsis "functional XML parsing framework")
  (description
   "SSAX includes SAX/DOM and SXML parsers with"
   "support for XML Namespaces and validation."
   "It also provides tools for transforming the parsed SXML"
   "in way similar to XSLT.")
  (homepage "http://ssax.sourceforge.net/")
  
  (libraries
   (sls -> "wak")
   (("ssax" "private") -> ("wak" "ssax" "private"))))

;; Local Variables:
;; scheme-indent-styles: (pkg-list)
;; End:
