#!r6rs
;; Copyright 2009 Derick Eddington.  My MIT-style license is in the file named
;; LICENSE from the original collection this file is distributed with.

(import
  (except (rnrs) assert)
  (wak ssax tree-trans)
  (wak private include)
  (srfi :78 lightweight-testing)
  (srfi :48 intermediate-format-strings)
  (wak ssax private output))

(define-syntax assert
  (syntax-rules ()
    ((_ expr ...)
     (begin (check expr => #T) ...))))

(define (pp x)
  (display "\nPretty Printed:\n")
  (format #t "~y" x)
  (newline))

(include-file ("wak" "ssax" "private" "tests") "vSXML-tree-trans.scm")

(check-report)
