#!r6rs
;; Copyright 2009 Derick Eddington.  My MIT-style license is in the file named
;; LICENSE from the original collection this file is distributed with.

(import
  (except (rnrs) assert)
  (wak ssax html)
  (wak ssax tree-trans)
  (wak private include)
  (srfi :78 lightweight-testing)
  (wak ssax private output)
  (wak ssax private misc))

(define-syntax assert
  (syntax-rules ()
    ((_ expr ...)
     (begin (check expr => #T) ...))))

(include-file ("wak" "ssax" "private" "tests") "vSXML-to-HTML.scm")

(check-report)
